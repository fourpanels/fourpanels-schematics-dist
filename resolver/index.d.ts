import { Rule } from '@angular-devkit/schematics';
import { Schema as ResolverOptions } from './schema.model';
export default function (options: ResolverOptions): Rule;
