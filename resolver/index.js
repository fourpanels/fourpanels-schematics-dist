"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const strings_1 = require("@angular-devkit/core/src/utils/strings");
const schematics_1 = require("@angular-devkit/schematics");
function default_1(options) {
    return (tree, _context) => {
        const classifyName = strings_1.classify(options.name);
        const dasherizeName = strings_1.dasherize(options.name);
        const moduleName = strings_1.dasherize(options.module);
        const routeDir = moduleName === '' || moduleName === 'app' ?
            `/apps/next-web/app/route` :
            `/apps/next-web/app/modules/${moduleName}/route`;
        if (!tree.exists(`${routeDir}/${moduleName}-routing.module.ts`)) {
            throw new schematics_1.SchematicsException(`route module is not defined in (${routeDir}/${moduleName}-routing.module.ts)`);
        }
        const templateSource = schematics_1.apply(schematics_1.url('./files'), [
            schematics_1.applyTemplates({ classifyName, dasherizeName }),
            schematics_1.move(routeDir)
        ]);
        return schematics_1.mergeWith(templateSource)(tree, _context);
    };
}
exports.default = default_1;
//# sourceMappingURL=index.js.map