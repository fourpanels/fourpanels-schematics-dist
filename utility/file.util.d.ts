import * as ts from 'typescript';
import { Tree } from '@angular-devkit/schematics';
export declare function getSourceFile(host: Tree, path: string): ts.SourceFile;
export declare function copyFile(tree: Tree, from: string, to: string): void;
export declare function removeFile(tree: Tree, path: string): void;
export declare function renameFile(tree: Tree, path: string, newPath: string): void;
