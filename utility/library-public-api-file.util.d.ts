import { Tree } from "@angular-devkit/schematics";
export declare function addExportModuleToLibraryPublicApi(tree: Tree, moduleName: string, pathToExport: string): import("@angular-devkit/schematics/src/tree/interface").Tree;
