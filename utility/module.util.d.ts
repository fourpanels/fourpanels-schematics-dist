import { Path } from "@angular-devkit/core";
import { Tree } from "@angular-devkit/schematics";
import { GenerateNewFileType, PlatformType } from "../global-model";
/**
 * If module name start with `@lib/`, then module is a library module.
 */
export declare function isLibraryModule(module: string): boolean;
/**
 * If module is a library, then return module name with `lib-`prefix.
 *
 * @example
 * `@lib/button` ->  `lib-button`
 * `messenger`   ->  `messenger`
 */
export declare function getModuleNameFromOption({ module }: {
    module: string;
}): string;
/**
 * Throw an error Platform Type of new file and module don't match.
 * @return modulePath and moduleDeclarationsArrayName
 *
 */
export declare function validateModuleTypeAndGetModuleInfo(tree: Tree, dirPath: string, moduleName: string, newFilePlatformType: PlatformType, newFileType: GenerateNewFileType): {
    modulePath: Path;
    moduleDeclarationsArrayName: string;
};
/**
 * Check if new file type (i.e component in web) can add to his own module.
 * Throw an Error if platformTypes don't match.
 *
 * i.e. A web component can not add to ns module.
 *
 * @param modulePlatformType PlatformType of module
 * @param newFilePlatformType PlatformType of new file (i.e. new file is component and platform type of it is web)
 * @param newFileType What king of file is used (i.e. Component, Directive, ...). It is just for print pretty error.
 *
 */
export declare function validatePlatformTypes(modulePlatformType: PlatformType, newFilePlatformType: PlatformType, newFileType: GenerateNewFileType): void;
/**
 * @param newFilePlatformType PlatformType of new file
 * @returns moduleExtension (e.g. `.tns`) and moduleDeclarationsArrayName (e.g. `commonModuleDeclarations`)
 */
export declare function getModuleInfo(newFilePlatformType: PlatformType): {
    moduleExtension: string;
    moduleDeclarationsArrayName: string;
};
/**
 *
 * @param tree
 * @param path
 * @param moduleName
 * @returns
 */
export declare function getModulePlatformTypeFromPath(tree: Tree, path: string, moduleName: string): PlatformType;
/**
 * Maybe we need it later
 */
