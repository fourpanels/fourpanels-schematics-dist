"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.insertImport = void 0;
const file_util_1 = require("./file.util");
const ast_utils_1 = require("@schematics/angular/utility/ast-utils");
const ts = require("typescript");
function insertImport(tree, fileToEdit, symbolName, fileName) {
    const sourceFile = file_util_1.getSourceFile(tree, fileToEdit);
    const allImports = ast_utils_1.findNodes(sourceFile, ts.SyntaxKind.ImportDeclaration);
    const positionToAdd = allImports.length > 0 ? allImports[allImports.length - 1].end : 0;
    const importStrToAdd = allImports.length > 0
        ? `\nimport { ${symbolName} } from '${fileName}';`
        : `import { ${symbolName} } from '${fileName}';\n\n`;
    const exportRecorder = tree.beginUpdate(fileToEdit);
    exportRecorder.insertLeft(positionToAdd, importStrToAdd);
    tree.commitUpdate(exportRecorder);
}
exports.insertImport = insertImport;
//# sourceMappingURL=insert-import.util.js.map