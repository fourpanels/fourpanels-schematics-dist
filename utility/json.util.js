"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.updateJsonFile = void 0;
const core_1 = require("@angular-devkit/core");
function updateJsonFile(tree, path, callback) {
    const source = tree.read(path);
    if (source) {
        const sourceText = source.toString('utf-8');
        const json = core_1.parseJson(sourceText, core_1.JsonParseMode.Loose);
        callback(json);
        tree.overwrite(path, JSON.stringify(json, null, 2));
    }
    return tree;
}
exports.updateJsonFile = updateJsonFile;
//# sourceMappingURL=json.util.js.map