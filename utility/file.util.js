"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.renameFile = exports.removeFile = exports.copyFile = exports.getSourceFile = void 0;
// import * as ts from '../../node_modules/@schematics/angular/third_party/github.com/Microsoft/TypeScript/lib/typescript';
const ts = require("typescript");
const schematics_1 = require("@angular-devkit/schematics");
function getSourceFile(host, path) {
    const buffer = host.read(path);
    if (!buffer) {
        throw new schematics_1.SchematicsException(`GetSourceFile: Could not find ${path}.`);
    }
    const content = buffer.toString();
    const source = ts.createSourceFile(path, content, ts.ScriptTarget.Latest, true);
    return source;
}
exports.getSourceFile = getSourceFile;
function copyFile(tree, from, to) {
    const file = tree.get(from);
    if (!file) {
        throw new Error(`CopyFile: ${from} does not exist!`);
    }
    tree.create(to, file.content);
}
exports.copyFile = copyFile;
;
function removeFile(tree, path) {
    if (!tree.exists(path)) {
        throw new Error(`RemoveFile: ${path} does not exist!`);
    }
    tree.delete(path);
}
exports.removeFile = removeFile;
;
function renameFile(tree, path, newPath) {
    if (!tree.exists(path)) {
        throw new Error(`RenameFile: ${path} does not exist!`);
    }
    tree.rename(path, newPath);
}
exports.renameFile = renameFile;
//# sourceMappingURL=file.util.js.map