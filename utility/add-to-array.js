"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.addToArray = void 0;
const schematics_1 = require("@angular-devkit/schematics");
const ts = require("typescript");
const change_1 = require("@schematics/angular/utility/change");
const find_util_1 = require("./find.util");
// import { findByVariableName } from "./find-by-variable-name.util";
function addToArray(tree, path, variableName, value) {
    let change = addToArrayChange(path, variableName, value, tree);
    const declarationRecorder = tree.beginUpdate(path);
    if (change instanceof change_1.InsertChange) {
        declarationRecorder.insertLeft(change.pos, change.toAdd);
    }
    tree.commitUpdate(declarationRecorder); // commits the update on the tree
}
exports.addToArray = addToArray;
function addToArrayChange(path, variableName, inputValue, tree) {
    const { targetNodeSiblings } = find_util_1.findByVariableName(tree, path, variableName);
    const targetArrayNode = getArrayNode(targetNodeSiblings);
    const value = getNewArrayValue(inputValue, targetArrayNode);
    // insert the new value to the end of the array
    return new change_1.InsertChange(path, targetArrayNode.getEnd(), value);
}
function getArrayNode(targetNodeSiblings) {
    // get target array literal expression from the siblings, this means this sign "["
    let targetArrayLiteralExpressionNode = targetNodeSiblings.find(n => n.kind === ts.SyntaxKind.ArrayLiteralExpression);
    if (!targetArrayLiteralExpressionNode) {
        throw new schematics_1.SchematicsException(`target ArrayLiteralExpression node is not defined`);
    }
    // get target array list node which is in the children of targetArrayLiteralExpressionNode and its kind of SyntaxList
    const targetArrayNode = targetArrayLiteralExpressionNode.getChildren().find(n => n.kind === ts.SyntaxKind.SyntaxList);
    if (!targetArrayNode) {
        throw new schematics_1.SchematicsException(`target list node is not defined`);
    }
    return targetArrayNode;
}
function getNewArrayValue(inputValue, targetArrayNode) {
    let value = '';
    const targetListTrim = targetArrayNode.getText().toString().trim().replace(/\t|\n/g, '');
    if (targetListTrim.length > 1 && targetListTrim[targetListTrim.length - 1] !== ',') {
        value += ',';
    }
    value += '\n  ' + inputValue;
    return value;
}
//# sourceMappingURL=add-to-array.js.map