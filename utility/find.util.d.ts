import { Tree } from "@angular-devkit/schematics";
import * as ts from "typescript";
interface FindByVariableNameResult {
    targetNode: ts.Node;
    targetNodeSiblings: ts.Node[];
}
export declare function findByVariableName(tree: Tree, path: string, variableName: string): FindByVariableNameResult;
export declare function findInFile(tree: Tree, path: string, findFn: (value: ts.Node, index?: number) => boolean): ts.Node;
export {};
