"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getModulePlatformTypeFromPath = exports.getModuleInfo = exports.validatePlatformTypes = exports.validateModuleTypeAndGetModuleInfo = exports.getModuleNameFromOption = exports.isLibraryModule = void 0;
const core_1 = require("@angular-devkit/core");
const strings_1 = require("@angular-devkit/core/src/utils/strings");
const global_model_1 = require("../global-model");
/**
 * If module name start with `@lib/`, then module is a library module.
 */
function isLibraryModule(module) {
    return module.split('/')[0] === '@lib';
}
exports.isLibraryModule = isLibraryModule;
/**
 * If module is a library, then return module name with `lib-`prefix.
 *
 * @example
 * `@lib/button` ->  `lib-button`
 * `messenger`   ->  `messenger`
 */
function getModuleNameFromOption({ module }) {
    return isLibraryModule(module) ?
        `lib-${module.split('/')[1]}` :
        module;
}
exports.getModuleNameFromOption = getModuleNameFromOption;
/**
 * Throw an error Platform Type of new file and module don't match.
 * @return modulePath and moduleDeclarationsArrayName
 *
 */
function validateModuleTypeAndGetModuleInfo(tree, dirPath, moduleName, newFilePlatformType, newFileType) {
    const moduleType = getModulePlatformTypeFromPath(tree, dirPath, moduleName);
    validatePlatformTypes(moduleType, newFilePlatformType, newFileType);
    const { moduleExtension, moduleDeclarationsArrayName } = getModuleInfo(newFilePlatformType);
    const modulePath = findModule(tree, dirPath, moduleExtension, moduleName);
    if (!modulePath) {
        throw new Error(`Specified module does not exist \n moduleName: ${moduleName} \n path: ${dirPath}`);
    }
    return {
        modulePath,
        moduleDeclarationsArrayName
    };
}
exports.validateModuleTypeAndGetModuleInfo = validateModuleTypeAndGetModuleInfo;
/**
 * Check if new file type (i.e component in web) can add to his own module.
 * Throw an Error if platformTypes don't match.
 *
 * i.e. A web component can not add to ns module.
 *
 * @param modulePlatformType PlatformType of module
 * @param newFilePlatformType PlatformType of new file (i.e. new file is component and platform type of it is web)
 * @param newFileType What king of file is used (i.e. Component, Directive, ...). It is just for print pretty error.
 *
 */
function validatePlatformTypes(modulePlatformType, newFilePlatformType, newFileType) {
    // Module: Shared
    if (modulePlatformType === global_model_1.PlatformType.Shared) {
        return;
    }
    // Module: Mobile / Decoration: Mobile
    if (modulePlatformType === global_model_1.PlatformType.Ns && newFilePlatformType === global_model_1.PlatformType.Ns) {
        return;
    }
    // Module: Web / Decoration: Web
    if (modulePlatformType === global_model_1.PlatformType.Web && newFilePlatformType === global_model_1.PlatformType.Web) {
        return;
    }
    const newFileTypeDasherize = strings_1.dasherize(newFileType);
    const newFileTypeClassify = strings_1.classify(newFileType);
    // Shared Component to Ns Module
    if (modulePlatformType === global_model_1.PlatformType.Ns && newFilePlatformType === global_model_1.PlatformType.Shared) {
        throw new Error(`Invalid platform Type: The module is just for Nativescript, but the ${newFileTypeDasherize} is a Shared ${newFileTypeClassify}`);
    }
    // Shared Component to Ns Module
    if (modulePlatformType === global_model_1.PlatformType.Ns && newFilePlatformType === global_model_1.PlatformType.Web) {
        throw new Error(`Invalid platform Type: The module is just for Nativescript, but the ${newFileTypeDasherize} is a Web ${newFileTypeClassify}`);
    }
    // Ns Component to Web Module
    if (modulePlatformType === global_model_1.PlatformType.Web && newFilePlatformType === global_model_1.PlatformType.Ns) {
        throw new Error(`Invalid platform Type: The module is just for Web, but the ${newFileTypeDasherize} is a Nativescript ${newFileTypeClassify}`);
    }
    // Shared Component to Web Module
    if (modulePlatformType === global_model_1.PlatformType.Web && newFilePlatformType === global_model_1.PlatformType.Shared) {
        throw new Error(`Invalid platform Type: The module is just for Web, but the ${newFileTypeDasherize} is a Shared ${newFileTypeClassify}`);
    }
}
exports.validatePlatformTypes = validatePlatformTypes;
/**
 * @param newFilePlatformType PlatformType of new file
 * @returns moduleExtension (e.g. `.tns`) and moduleDeclarationsArrayName (e.g. `commonModuleDeclarations`)
 */
function getModuleInfo(newFilePlatformType) {
    // Web only
    if (newFilePlatformType === global_model_1.PlatformType.Web) {
        return { moduleExtension: global_model_1.WEB_EXTENSION, moduleDeclarationsArrayName: global_model_1.WEB_COMPONENT_DECLARATIONS_ARRAY_NAME };
    }
    // Ns only
    if (newFilePlatformType === global_model_1.PlatformType.Ns) {
        return { moduleExtension: global_model_1.NS_EXTENSION, moduleDeclarationsArrayName: global_model_1.NS_COMPONENT_DECLARATIONS_ARRAY_NAME };
    }
    // Shared
    if (newFilePlatformType === global_model_1.PlatformType.Shared) {
        return { moduleExtension: global_model_1.COMMON_EXTENSION, moduleDeclarationsArrayName: global_model_1.COMMON_COMPONENT_DECLARATIONS_ARRAY_NAME };
    }
}
exports.getModuleInfo = getModuleInfo;
/**
 *
 * @param tree
 * @param path
 * @param moduleName
 * @returns
 */
function getModulePlatformTypeFromPath(tree, path, moduleName) {
    const commonModule = findModule(tree, path, global_model_1.COMMON_EXTENSION, moduleName);
    const webModule = findModule(tree, path, global_model_1.WEB_EXTENSION, moduleName);
    const nsModule = findModule(tree, path, global_model_1.NS_EXTENSION, moduleName);
    if (commonModule && webModule && nsModule) {
        return global_model_1.PlatformType.Shared;
    }
    if (!commonModule && webModule && nsModule) {
        return global_model_1.PlatformType.Ns;
    }
    if (!commonModule && webModule && !nsModule) {
        return global_model_1.PlatformType.Web;
    }
}
exports.getModulePlatformTypeFromPath = getModulePlatformTypeFromPath;
function findModule(tree, path, extension, moduleName) {
    while (path && path !== '/') {
        const modulePath = core_1.normalize(`/${path}/${moduleName}`);
        const moduleBaseName = core_1.normalize(modulePath).split('/').pop();
        if (tree.exists(modulePath)) {
            return core_1.normalize(modulePath);
        }
        else if (tree.exists(modulePath + extension + '.ts')) {
            return core_1.normalize(modulePath + extension + '.ts');
        }
        else if (tree.exists(modulePath + '.module' + extension + '.ts')) {
            return core_1.normalize(modulePath + '.module' + extension + '.ts');
        }
        else if (tree.exists(modulePath + '/' + moduleBaseName + '.module' + extension + '.ts')) {
            return core_1.normalize(modulePath + '/' + moduleBaseName + '.module' + extension + '.ts');
        }
        path = core_1.dirname(path);
    }
    return null;
}
/**
 * Maybe we need it later
 */
// function addDeclarationToNgModule(tree: Tree, modulePath: Path, className: string, classImportPath: string) {
//     const sourceFile = getSourceFile(tree, modulePath);
//     const declarationChanges = ngUtil.astUtils.addDeclarationToModule(sourceFile, modulePath, className, classImportPath);
//     const declarationRecorder = tree.beginUpdate(modulePath);
//     for (const change of declarationChanges) {
//         if (change instanceof ngUtil.change.InsertChange) {
//             declarationRecorder.insertLeft(change.pos, change.toAdd);
//         }
//     }
//     tree.commitUpdate(declarationRecorder);
// }
//# sourceMappingURL=module.util.js.map