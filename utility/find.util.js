"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.findInFile = exports.findByVariableName = void 0;
const schematics_1 = require("@angular-devkit/schematics");
const ts = require("typescript");
const node_util_1 = require("./node.util");
function findByVariableName(tree, path, variableName) {
    const targetNode = findInFile(tree, path, n => n.kind === ts.SyntaxKind.Identifier && n.getText() === variableName);
    if (!targetNode || !targetNode.parent) {
        throw new schematics_1.SchematicsException(`expected ${variableName} variable in ${path}`);
    }
    // define peopleNode's sibling nodes and remove peopleNode from it
    let targetNodeSiblings = targetNode.parent.getChildren();
    let targetNodeIndex = targetNodeSiblings.indexOf(targetNode);
    targetNodeSiblings = targetNodeSiblings.slice(targetNodeIndex);
    return {
        targetNode,
        targetNodeSiblings
    };
}
exports.findByVariableName = findByVariableName;
function findInFile(tree, path, findFn) {
    let text = tree.read(path); // reads the file from the tree
    if (!text)
        throw new schematics_1.SchematicsException(`File ${path} does not exist.`); // throw an error if the file doesn't exist
    let sourceText = text.toString('utf-8');
    // create the typescript source file
    let sourceFile = ts.createSourceFile('dummy-file', sourceText, ts.ScriptTarget.Latest, true);
    // get the nodes of the source file
    let nodes = node_util_1.getSourceNodes(sourceFile);
    const targetNode = nodes.find(findFn);
    return targetNode;
}
exports.findInFile = findInFile;
//# sourceMappingURL=find.util.js.map