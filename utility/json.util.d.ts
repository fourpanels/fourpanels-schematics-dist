import { Tree } from "@angular-devkit/schematics";
interface UpdateJsonFn<T> {
    (obj: T): T | void;
}
export declare function updateJsonFile<T>(tree: Tree, path: string, callback: UpdateJsonFn<T>): Tree;
export {};
