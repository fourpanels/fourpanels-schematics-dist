"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.addExportModuleToLibraryPublicApi = void 0;
const ast_utils_1 = require("@schematics/angular/utility/ast-utils");
const change_1 = require("@schematics/angular/utility/change");
const file_util_1 = require("./file.util");
function addExportModuleToLibraryPublicApi(tree, moduleName, pathToExport) {
    const publicApiFile = `libs/${moduleName}/src/public-api.ts`;
    const stingToInsert = `\nexport * from '${pathToExport}';`;
    const sourceFile = file_util_1.getSourceFile(tree, publicApiFile);
    const sourceNodes = ast_utils_1.getSourceNodes(sourceFile);
    const exportChanges = ast_utils_1.insertAfterLastOccurrence(sourceNodes, stingToInsert, publicApiFile, sourceNodes.length);
    const exportRecorder = tree.beginUpdate(publicApiFile);
    if (exportChanges instanceof change_1.InsertChange) {
        exportRecorder.insertLeft(exportChanges.pos, exportChanges.toAdd);
    }
    tree.commitUpdate(exportRecorder);
    return tree;
}
exports.addExportModuleToLibraryPublicApi = addExportModuleToLibraryPublicApi;
//# sourceMappingURL=library-public-api-file.util.js.map