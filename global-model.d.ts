export declare const WEB_EXTENSION = "";
export declare const NS_EXTENSION = ".tns";
export declare const COMMON_EXTENSION = ".common";
export declare const COMMON_COMPONENT_DECLARATIONS_ARRAY_NAME = "commonModuleDeclarations";
export declare const NS_COMPONENT_DECLARATIONS_ARRAY_NAME = "nsModuleDeclarations";
export declare const WEB_COMPONENT_DECLARATIONS_ARRAY_NAME = "webModuleDeclarations";
export declare enum PlatformType {
    Shared = "shared",
    Web = "web",
    Ns = "ns"
}
export declare enum GenerateNewFileType {
    Component = "Component",
    Directive = "Directive",
    Pipe = "Pipe"
}
