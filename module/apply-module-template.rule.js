"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.applySharedModuleTemplate = exports.applyNsModuleTemplate = exports.applyWebModuleTemplate = exports.applyModuleTemplate = void 0;
const schematics_1 = require("@angular-devkit/schematics");
const global_model_1 = require("../global-model");
function applyModuleTemplate(paredModuleInfo) {
    switch (paredModuleInfo.platformType) {
        case global_model_1.PlatformType.Web:
            return applyWebModuleTemplate(paredModuleInfo);
        case global_model_1.PlatformType.Ns:
            return applyNsModuleTemplate(paredModuleInfo);
        case global_model_1.PlatformType.Shared:
            return applySharedModuleTemplate(paredModuleInfo);
    }
}
exports.applyModuleTemplate = applyModuleTemplate;
;
function applyWebModuleTemplate(paredModuleInfo) {
    return (tree, context) => {
        const templateSource = schematics_1.apply(schematics_1.url('./module-web-files'), [
            schematics_1.applyTemplates(paredModuleInfo),
            schematics_1.move(paredModuleInfo.dirPath)
        ]);
        return schematics_1.mergeWith(templateSource)(tree, context);
    };
}
exports.applyWebModuleTemplate = applyWebModuleTemplate;
;
function applyNsModuleTemplate(paredModuleInfo) {
    return (tree, context) => {
        const templateSource = schematics_1.apply(schematics_1.url('./module-ns-files'), [
            schematics_1.applyTemplates(paredModuleInfo),
            schematics_1.move(paredModuleInfo.dirPath)
        ]);
        return schematics_1.mergeWith(templateSource)(tree, context);
    };
}
exports.applyNsModuleTemplate = applyNsModuleTemplate;
;
function applySharedModuleTemplate(paredModuleInfo) {
    return (tree, context) => {
        const templateSource = schematics_1.apply(schematics_1.url('./module-shared-files'), [
            schematics_1.applyTemplates(paredModuleInfo),
            schematics_1.move(paredModuleInfo.dirPath)
        ]);
        return schematics_1.mergeWith(templateSource)(tree, context);
    };
}
exports.applySharedModuleTemplate = applySharedModuleTemplate;
;
//# sourceMappingURL=apply-module-template.rule.js.map