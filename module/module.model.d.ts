import { PlatformType } from "../global-model";
export declare const MODULE_FOLDER_PATH = "/src/app/modules/";
export interface ParsedModuleInfo {
    routing: boolean;
    name: string;
    classifyName: string;
    dirPath: string;
    platformType: PlatformType;
}
export interface ModuleSchema {
    /**
     * The name of the module.
     */
    name: string;
    /**
     * Module will be used in Web, Ns or both platform.
     */
    platformType: PlatformType;
    /**
     * When true, creates a routing module.
     */
    routing: boolean;
}
