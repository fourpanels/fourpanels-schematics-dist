import { ModuleSchema, ParsedModuleInfo } from "./module.model";
export declare const parseModuleInfo: (options: ModuleSchema) => ParsedModuleInfo;
