"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.parseModuleInfo = void 0;
const strings_1 = require("@angular-devkit/core/src/utils/strings");
exports.parseModuleInfo = (options) => {
    return {
        routing: options.routing,
        name: strings_1.dasherize(options.name),
        classifyName: strings_1.classify(options.name),
        dirPath: `/apps/next-web/app/modules/${strings_1.dasherize(options.name)}`,
        platformType: options.platformType
    };
};
//# sourceMappingURL=pars-module-info.js.map