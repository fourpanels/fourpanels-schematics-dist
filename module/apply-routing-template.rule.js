"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.applySharedRoutingTemplate = exports.applyNsRoutingTemplate = exports.applyWebRoutingTemplate = exports.applyRoutingTemplate = void 0;
const schematics_1 = require("@angular-devkit/schematics");
const global_model_1 = require("../global-model");
function applyRoutingTemplate(paredModuleInfo) {
    if (!paredModuleInfo.routing) {
        return (tree, context) => tree;
    }
    switch (paredModuleInfo.platformType) {
        case global_model_1.PlatformType.Web:
            return applyWebRoutingTemplate(paredModuleInfo);
        case global_model_1.PlatformType.Ns:
            return applyNsRoutingTemplate(paredModuleInfo);
        case global_model_1.PlatformType.Shared:
            return applySharedRoutingTemplate(paredModuleInfo);
    }
}
exports.applyRoutingTemplate = applyRoutingTemplate;
;
function applyWebRoutingTemplate(paredModuleInfo) {
    return (tree, context) => {
        const templateSource = schematics_1.apply(schematics_1.url('./routing-web-files'), [
            schematics_1.applyTemplates(paredModuleInfo),
            schematics_1.move(`${paredModuleInfo.dirPath}/route/`)
        ]);
        return schematics_1.mergeWith(templateSource)(tree, context);
    };
}
exports.applyWebRoutingTemplate = applyWebRoutingTemplate;
;
function applyNsRoutingTemplate(paredModuleInfo) {
    return (tree, context) => {
        const templateSource = schematics_1.apply(schematics_1.url('./routing-ns-files'), [
            schematics_1.applyTemplates(paredModuleInfo),
            schematics_1.move(`${paredModuleInfo.dirPath}/route/`)
        ]);
        return schematics_1.mergeWith(templateSource)(tree, context);
    };
}
exports.applyNsRoutingTemplate = applyNsRoutingTemplate;
;
function applySharedRoutingTemplate(paredModuleInfo) {
    return (tree, context) => {
        const templateSource = schematics_1.apply(schematics_1.url('./routing-shared-files'), [
            schematics_1.applyTemplates(paredModuleInfo),
            schematics_1.move(`${paredModuleInfo.dirPath}/route`)
        ]);
        return schematics_1.mergeWith(templateSource)(tree, context);
    };
}
exports.applySharedRoutingTemplate = applySharedRoutingTemplate;
;
//# sourceMappingURL=apply-routing-template.rule.js.map