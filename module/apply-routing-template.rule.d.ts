import { Rule } from "@angular-devkit/schematics";
import { ParsedModuleInfo } from "./module.model";
export declare function applyRoutingTemplate(paredModuleInfo: ParsedModuleInfo): Rule;
export declare function applyWebRoutingTemplate(paredModuleInfo: ParsedModuleInfo): Rule;
export declare function applyNsRoutingTemplate(paredModuleInfo: ParsedModuleInfo): Rule;
export declare function applySharedRoutingTemplate(paredModuleInfo: ParsedModuleInfo): Rule;
