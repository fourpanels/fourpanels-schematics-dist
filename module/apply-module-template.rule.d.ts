import { Rule } from "@angular-devkit/schematics";
import { ParsedModuleInfo } from "./module.model";
export declare function applyModuleTemplate(paredModuleInfo: ParsedModuleInfo): Rule;
export declare function applyWebModuleTemplate(paredModuleInfo: ParsedModuleInfo): Rule;
export declare function applyNsModuleTemplate(paredModuleInfo: ParsedModuleInfo): Rule;
export declare function applySharedModuleTemplate(paredModuleInfo: ParsedModuleInfo): Rule;
