import { Rule } from '@angular-devkit/schematics';
import { ModuleSchema } from './module.model';
export default function (options: ModuleSchema): Rule;
