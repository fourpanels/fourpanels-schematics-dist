"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const schematics_1 = require("@angular-devkit/schematics");
const apply_module_template_rule_1 = require("./apply-module-template.rule");
const apply_routing_template_rule_1 = require("./apply-routing-template.rule");
const pars_module_info_1 = require("./pars-module-info");
function default_1(options) {
    const paredModuleInfo = pars_module_info_1.parseModuleInfo(options);
    return (tree, _context) => {
        return schematics_1.chain([
            apply_module_template_rule_1.applyModuleTemplate(paredModuleInfo),
            apply_routing_template_rule_1.applyRoutingTemplate(paredModuleInfo),
        ]);
    };
}
exports.default = default_1;
//# sourceMappingURL=index.js.map