import { Rule } from '@angular-devkit/schematics';
import { ServiceSchema } from './service.model';
export default function (options: ServiceSchema): Rule;
