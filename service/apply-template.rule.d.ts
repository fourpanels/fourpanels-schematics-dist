import { Rule } from "@angular-devkit/schematics";
import { ParsedServiceInfo } from "./service.model";
export declare function applyTemplate(paredServiceInfo: ParsedServiceInfo): Rule;
