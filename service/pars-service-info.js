"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.parseServiceInfo = void 0;
const strings_1 = require("@angular-devkit/core/src/utils/strings");
const module_util_1 = require("../utility/module.util");
exports.parseServiceInfo = (options) => {
    const isLibService = module_util_1.isLibraryModule(options.module);
    const moduleName = module_util_1.getModuleNameFromOption(options);
    const name = getNameFromOption(options);
    const classifyName = isLibService ? `Lib${strings_1.classify(options.name)}` : `${strings_1.classify(options.name)}`;
    const isAppModule = moduleName === '' || moduleName === 'app';
    const dirPath = isLibService ?
        `libs/${moduleName}/src/lib` :
        isAppModule ?
            `/apps/next-web/app/core` :
            `/apps/next-web/app/modules/${moduleName}`;
    return {
        isLibService,
        name,
        classifyName,
        moduleName,
        dirPath
    };
};
// ----- Utils -----
function getNameFromOption({ module, name }) {
    return module_util_1.isLibraryModule(module) ? `lib-${strings_1.dasherize(name)}` : strings_1.dasherize(name);
}
//# sourceMappingURL=pars-service-info.js.map