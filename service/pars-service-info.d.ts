import { ParsedServiceInfo, ServiceSchema } from "./service.model";
export declare const parseServiceInfo: (options: ServiceSchema) => ParsedServiceInfo;
