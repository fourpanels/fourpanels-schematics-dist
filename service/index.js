"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const schematics_1 = require("@angular-devkit/schematics");
const apply_template_rule_1 = require("./apply-template.rule");
const pars_service_info_1 = require("./pars-service-info");
function default_1(options) {
    const parsedServiceInfo = pars_service_info_1.parseServiceInfo(options);
    return (tree, _context) => {
        return schematics_1.chain([
            apply_template_rule_1.applyTemplate(parsedServiceInfo),
        ]);
    };
}
exports.default = default_1;
//# sourceMappingURL=index.js.map