export interface ParsedServiceInfo {
    isLibService: boolean;
    name: string;
    classifyName: string;
    moduleName: string;
    dirPath: string;
}
export interface ServiceSchema {
    name: string;
    module: string;
}
