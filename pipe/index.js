"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const schematics_1 = require("@angular-devkit/schematics");
const pars_pipe_info_1 = require("./pars-pipe-info");
const add_to_module_rule_1 = require("./add-to-module.rule");
const update_public_api_file_1 = require("./update-public-api-file");
const apply_template_rule_1 = require("./apply-template.rule");
function default_1(options) {
    const parsedPipeInfo = pars_pipe_info_1.parsePipeInfo(options);
    return schematics_1.chain([
        apply_template_rule_1.applyTemplate(parsedPipeInfo),
        add_to_module_rule_1.addToModule(parsedPipeInfo),
        update_public_api_file_1.updateLibraryPublicApiFile(parsedPipeInfo)
    ]);
}
exports.default = default_1;
//# sourceMappingURL=index.js.map