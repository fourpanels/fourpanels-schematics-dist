"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.updateLibraryPublicApiFile = void 0;
const library_public_api_file_util_1 = require("../utility/library-public-api-file.util");
function updateLibraryPublicApiFile(paredPipeInfo) {
    // Pipe is not library
    if (!paredPipeInfo.isLibPipe) {
        return (tree) => tree;
    }
    return (tree) => {
        const pathToExport = `./lib/pipes/${paredPipeInfo.name}.pipe`;
        library_public_api_file_util_1.addExportModuleToLibraryPublicApi(tree, paredPipeInfo.moduleName, pathToExport);
        return tree;
    };
}
exports.updateLibraryPublicApiFile = updateLibraryPublicApiFile;
;
//# sourceMappingURL=update-public-api-file.js.map