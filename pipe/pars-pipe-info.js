"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.parsePipeInfo = void 0;
const strings_1 = require("@angular-devkit/core/src/utils/strings");
const module_util_1 = require("../utility/module.util");
exports.parsePipeInfo = (options) => {
    const isLibPipe = module_util_1.isLibraryModule(options.module);
    const moduleName = module_util_1.getModuleNameFromOption(options);
    const name = getNameFromOption(options);
    const dirPath = isLibPipe ?
        `/libs/${moduleName}/src/lib/pipes` :
        `/apps/next-web/app/modules/${moduleName}/pipes`;
    const selector = strings_1.camelize(options.name);
    const className = `${strings_1.classify(selector)}Pipe`;
    const classPath = `${dirPath}/${name}.pipe.ts`;
    /**
     * Library
     * from: /libs/lib-button/src/lib/pipes/lib-duration.pipe.ts
     * to: ./pipes/lib-duration.pipe
     *
     * Module
     * from: /apps/next-web/app/modules/register/pipes/duration.pipe.ts
     * to: @src/app/modules/register/pipes/duration.pipe
     */
    const classPathToImport = isLibPipe ?
        classPath.split('/src/lib')[1].replace(/\.ts$/, '').replace('/', './') :
        classPath.replace('/apps/next-web', '@').replace(/\.ts$/, '');
    return {
        isLibPipe,
        moduleName,
        name,
        dirPath,
        selector,
        className,
        classPath,
        classPathToImport,
        platformType: options.platformType
    };
};
// ----- Utils -----
function getNameFromOption({ module, name }) {
    return module_util_1.isLibraryModule(module) ? `lib-${strings_1.dasherize(name)}` : strings_1.dasherize(name);
}
//# sourceMappingURL=pars-pipe-info.js.map