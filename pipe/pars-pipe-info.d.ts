import { ParsedPipeInfo, PipeSchema } from "./pipe.model";
export declare const parsePipeInfo: (options: PipeSchema) => ParsedPipeInfo;
