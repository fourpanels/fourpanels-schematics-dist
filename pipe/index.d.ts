import { Rule } from '@angular-devkit/schematics';
import { PipeSchema } from './pipe.model';
export default function (options: PipeSchema): Rule;
