import { PlatformType } from "../global-model";
export interface ParsedPipeInfo {
    isLibPipe: boolean;
    moduleName: string;
    name: string;
    dirPath: string;
    selector: string;
    className: string;
    classPath: string;
    classPathToImport: string;
    platformType: PlatformType;
}
export interface PipeSchema {
    name: string;
    module: string;
    platformType: PlatformType;
}
