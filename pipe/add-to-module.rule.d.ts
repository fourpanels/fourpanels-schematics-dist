import { Rule } from "@angular-devkit/schematics";
import { ParsedPipeInfo } from "./pipe.model";
export declare function addToModule(parsedPipeInfo: ParsedPipeInfo): Rule;
