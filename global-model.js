"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GenerateNewFileType = exports.PlatformType = exports.WEB_COMPONENT_DECLARATIONS_ARRAY_NAME = exports.NS_COMPONENT_DECLARATIONS_ARRAY_NAME = exports.COMMON_COMPONENT_DECLARATIONS_ARRAY_NAME = exports.COMMON_EXTENSION = exports.NS_EXTENSION = exports.WEB_EXTENSION = void 0;
exports.WEB_EXTENSION = '';
exports.NS_EXTENSION = '.tns';
exports.COMMON_EXTENSION = '.common';
exports.COMMON_COMPONENT_DECLARATIONS_ARRAY_NAME = 'commonModuleDeclarations';
exports.NS_COMPONENT_DECLARATIONS_ARRAY_NAME = 'nsModuleDeclarations';
exports.WEB_COMPONENT_DECLARATIONS_ARRAY_NAME = 'webModuleDeclarations';
var PlatformType;
(function (PlatformType) {
    PlatformType["Shared"] = "shared";
    PlatformType["Web"] = "web";
    PlatformType["Ns"] = "ns";
})(PlatformType = exports.PlatformType || (exports.PlatformType = {}));
var GenerateNewFileType;
(function (GenerateNewFileType) {
    GenerateNewFileType["Component"] = "Component";
    GenerateNewFileType["Directive"] = "Directive";
    GenerateNewFileType["Pipe"] = "Pipe";
})(GenerateNewFileType = exports.GenerateNewFileType || (exports.GenerateNewFileType = {}));
//# sourceMappingURL=global-model.js.map