"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const strings_1 = require("@angular-devkit/core/src/utils/strings");
const schematics_1 = require("@angular-devkit/schematics");
function default_1(options) {
    return (tree, _context) => {
        const moduleName = strings_1.dasherize(options.module);
        const routeDir = moduleName === '' || moduleName === 'app' ?
            `/apps/next-web/app/route` :
            `/apps/next-web/app/modules/${moduleName}/route`;
        if (!tree.exists(`${routeDir}/${moduleName}-routing.module.ts`)) {
            throw new schematics_1.SchematicsException(`Route module is not defined in (${routeDir}/${moduleName}-routing.module.ts)`);
        }
        options.path = routeDir;
        return schematics_1.externalSchematic('@schematics/angular', 'guard', Object.assign(Object.assign({}, options), { skipImport: true }));
    };
}
exports.default = default_1;
//# sourceMappingURL=index.js.map