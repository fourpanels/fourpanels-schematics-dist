import { Rule } from '@angular-devkit/schematics';
import { Schema as GuardOptions } from './schema.model';
export default function (options: GuardOptions): Rule;
