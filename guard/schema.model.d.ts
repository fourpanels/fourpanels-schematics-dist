export interface Schema {
    name: string;
    module: string;
    spec: boolean;
    skipTests: boolean;
    flat: boolean;
    path: string;
    project: string;
    lintFix: boolean;
    implements: ['CanActivate' | 'CanActivateChild' | 'CanDeactivate' | 'CanLoad'];
}
