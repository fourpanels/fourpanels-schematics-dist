"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.parseLibraryInfo = void 0;
const strings_1 = require("@angular-devkit/core/src/utils/strings");
exports.parseLibraryInfo = (options) => {
    const name = strings_1.dasherize(options.name);
    return {
        name: `${name}`,
        nameWithPrefix: `lib-${name}`,
        classifyNameWithPrefix: strings_1.classify(`lib-${name}`),
        nameWithScore: `@lib/${name}`,
        libDir: `libs/lib-${name}`,
        tsConfigPath: `libs/lib-${name}/src/public-api`,
        platformType: options.platformType,
    };
};
//# sourceMappingURL=pars-library-info.js.map