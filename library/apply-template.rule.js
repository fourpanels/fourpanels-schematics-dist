"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.applyModuleTemplate = exports.applyLibraryTemplate = void 0;
const schematics_1 = require("@angular-devkit/schematics");
const global_model_1 = require("../global-model");
function applyLibraryTemplate(paredLibraryInfo) {
    return (tree, context) => {
        const templateSource = schematics_1.apply(schematics_1.url('./lib-files'), [
            schematics_1.applyTemplates(paredLibraryInfo),
            schematics_1.move(paredLibraryInfo.libDir)
        ]);
        return schematics_1.mergeWith(templateSource)(tree, context);
    };
}
exports.applyLibraryTemplate = applyLibraryTemplate;
;
function applyModuleTemplate(paredLibraryInfo) {
    return (tree, context) => {
        let templateSource;
        switch (paredLibraryInfo.platformType) {
            case global_model_1.PlatformType.Web:
                templateSource = applyWebModuleTemplate(paredLibraryInfo);
                break;
            case global_model_1.PlatformType.Ns:
                templateSource = applyNsModuleTemplate(paredLibraryInfo);
                break;
            case global_model_1.PlatformType.Shared:
                templateSource = applySharedModuleTemplate(paredLibraryInfo);
                break;
            default:
                break;
        }
        return schematics_1.mergeWith(templateSource)(tree, context);
    };
}
exports.applyModuleTemplate = applyModuleTemplate;
;
function applyWebModuleTemplate(paredComponentInfo) {
    return schematics_1.apply(schematics_1.url('./module-web-files'), [
        schematics_1.applyTemplates(paredComponentInfo),
        schematics_1.move(`${paredComponentInfo.libDir}/src/lib`)
    ]);
}
function applyNsModuleTemplate(paredComponentInfo) {
    return schematics_1.apply(schematics_1.url('./module-ns-files'), [
        schematics_1.applyTemplates(paredComponentInfo),
        schematics_1.move(`${paredComponentInfo.libDir}/src/lib`)
    ]);
}
function applySharedModuleTemplate(paredComponentInfo) {
    return schematics_1.apply(schematics_1.url('./module-shared-files'), [
        schematics_1.applyTemplates(paredComponentInfo),
        schematics_1.move(`${paredComponentInfo.libDir}/src/lib`)
    ]);
}
//# sourceMappingURL=apply-template.rule.js.map