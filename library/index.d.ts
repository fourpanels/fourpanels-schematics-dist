import { Rule } from '@angular-devkit/schematics';
import { LibrarySchema } from './library.model';
export default function (options: LibrarySchema): Rule;
