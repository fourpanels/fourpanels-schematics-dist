import { Rule, Tree } from "@angular-devkit/schematics";
import { ParsedLibraryInfo } from "./library.model";
export declare function updateTsConfig(parsedLibraryInfo: ParsedLibraryInfo): Rule;
export declare function updateSingleTsConfig(tsConfigFile: string, packageName: string, distRoot: string): (tree: Tree) => import("@angular-devkit/schematics/src/tree/interface").Tree;
