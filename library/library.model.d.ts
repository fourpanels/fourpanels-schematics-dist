import { PlatformType } from "../global-model";
export interface ParsedLibraryInfo {
    name: string;
    nameWithPrefix: string;
    classifyNameWithPrefix: string;
    nameWithScore: string;
    libDir: string;
    tsConfigPath: string;
    platformType: PlatformType;
}
export interface LibrarySchema {
    /**
     * The name of the component.
     */
    name: string;
    /**
    * Component will be used in Web, Ns or both platform
    */
    platformType: PlatformType;
}
