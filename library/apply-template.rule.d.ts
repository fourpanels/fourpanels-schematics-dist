import { Rule } from "@angular-devkit/schematics";
import { ParsedLibraryInfo } from "./library.model";
export declare function applyLibraryTemplate(paredLibraryInfo: ParsedLibraryInfo): Rule;
export declare function applyModuleTemplate(paredLibraryInfo: ParsedLibraryInfo): Rule;
