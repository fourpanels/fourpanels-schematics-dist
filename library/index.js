"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const schematics_1 = require("@angular-devkit/schematics");
const pars_library_info_1 = require("./pars-library-info");
const update_ts_config_rule_1 = require("./update-ts-config.rule");
const apply_template_rule_1 = require("./apply-template.rule");
const install_task_rule_1 = require("./install-task.rule");
function default_1(options) {
    return (tree, _context) => {
        const parsedLibraryInfo = pars_library_info_1.parseLibraryInfo(options);
        return schematics_1.chain([
            apply_template_rule_1.applyLibraryTemplate(parsedLibraryInfo),
            apply_template_rule_1.applyModuleTemplate(parsedLibraryInfo),
            update_ts_config_rule_1.updateTsConfig(parsedLibraryInfo),
            install_task_rule_1.installLibraryTask()
        ]);
    };
}
exports.default = default_1;
//# sourceMappingURL=index.js.map