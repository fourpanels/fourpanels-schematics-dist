"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.installLibraryTask = void 0;
const tasks_1 = require("@angular-devkit/schematics/tasks");
function installLibraryTask() {
    return (tree, context) => {
        context.addTask(new tasks_1.NodePackageInstallTask());
    };
}
exports.installLibraryTask = installLibraryTask;
;
//# sourceMappingURL=install-task.rule.js.map