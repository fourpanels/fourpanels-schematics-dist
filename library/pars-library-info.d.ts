import { LibrarySchema, ParsedLibraryInfo } from "./library.model";
export declare const parseLibraryInfo: (options: LibrarySchema) => ParsedLibraryInfo;
