"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.updateSingleTsConfig = exports.updateTsConfig = void 0;
const json_util_1 = require("../utility/json.util");
const global_model_1 = require("../global-model");
function updateTsConfig(parsedLibraryInfo) {
    return (tree) => {
        // SHARED
        if (parsedLibraryInfo.platformType === global_model_1.PlatformType.Shared) {
            updateSingleTsConfig('tsconfig.json', parsedLibraryInfo.nameWithScore, parsedLibraryInfo.tsConfigPath)(tree);
            updateSingleTsConfig('tsconfig.app.json', parsedLibraryInfo.nameWithScore, parsedLibraryInfo.tsConfigPath)(tree);
            updateSingleTsConfig('tsconfig.tns.json', parsedLibraryInfo.nameWithScore, parsedLibraryInfo.tsConfigPath)(tree);
        }
        // WEB
        if (parsedLibraryInfo.platformType === global_model_1.PlatformType.Web) {
            updateSingleTsConfig('tsconfig.json', parsedLibraryInfo.nameWithScore, parsedLibraryInfo.tsConfigPath)(tree);
            updateSingleTsConfig('tsconfig.app.json', parsedLibraryInfo.nameWithScore, parsedLibraryInfo.tsConfigPath)(tree);
        }
        // NS
        if (parsedLibraryInfo.platformType === global_model_1.PlatformType.Ns) {
            updateSingleTsConfig('tsconfig.json', parsedLibraryInfo.nameWithScore, parsedLibraryInfo.tsConfigPath)(tree);
            updateSingleTsConfig('tsconfig.tns.json', parsedLibraryInfo.nameWithScore, parsedLibraryInfo.tsConfigPath)(tree);
        }
        return tree;
    };
}
exports.updateTsConfig = updateTsConfig;
function updateSingleTsConfig(tsConfigFile, packageName, distRoot) {
    return (tree) => {
        if (!tree.exists(tsConfigFile)) {
            return;
        }
        return json_util_1.updateJsonFile(tree, tsConfigFile, (tsconfig) => {
            if (!tsconfig.compilerOptions.paths) {
                tsconfig.compilerOptions.paths = {};
            }
            if (!tsconfig.compilerOptions.paths[packageName]) {
                tsconfig.compilerOptions.paths[packageName] = [];
            }
            tsconfig.compilerOptions.paths[packageName].push(distRoot);
        });
    };
}
exports.updateSingleTsConfig = updateSingleTsConfig;
;
//# sourceMappingURL=update-ts-config.rule.js.map