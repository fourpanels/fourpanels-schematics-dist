"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.updateLibraryPublicApiFile = void 0;
const library_public_api_file_util_1 = require("../utility/library-public-api-file.util");
function updateLibraryPublicApiFile(paredComponentInfo) {
    // Component is not library
    if (!paredComponentInfo.isLibComponent) {
        return (tree, context) => tree;
    }
    return (tree, context) => {
        const pathToExport = `./lib/components/${paredComponentInfo.name}/${paredComponentInfo.name}.component`;
        library_public_api_file_util_1.addExportModuleToLibraryPublicApi(tree, paredComponentInfo.moduleName, pathToExport);
        return tree;
    };
}
exports.updateLibraryPublicApiFile = updateLibraryPublicApiFile;
;
//# sourceMappingURL=update-public-api-file.rule.js.map