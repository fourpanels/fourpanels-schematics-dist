import { ComponentSchema, ParsedComponentInfo } from "./component.model";
export declare const parseComponentInfo: (options: ComponentSchema) => ParsedComponentInfo;
