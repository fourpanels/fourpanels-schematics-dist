"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.parseComponentInfo = void 0;
const strings_1 = require("@angular-devkit/core/src/utils/strings");
const global_model_1 = require("../global-model");
const module_util_1 = require("../utility/module.util");
exports.parseComponentInfo = (options) => {
    const isLibComponent = module_util_1.isLibraryModule(options.module);
    const moduleName = module_util_1.getModuleNameFromOption(options);
    const name = getNameFromOption(options);
    const dirPath = isLibComponent ?
        `/libs/${moduleName}/src/lib/components/${name}` :
        `/apps/next-web/app/modules/${moduleName}/components/${name}`;
    const webSelector = isLibComponent ? name : `app-${name}`;
    const nsSelector = strings_1.classify(webSelector);
    const className = `${strings_1.classify(name)}Component`;
    const classPath = `${dirPath}/${name}.component.ts`;
    /**
     * Library
     * from: /libs/lib-button/src/lib/components/lib-primary-button/lib-primary-button.component.ts
     * to: ./components/lib-primary-button/lib-primary-button.component
     *
     * Module
     * from: /apps/next-web/app/modules/register/components/register-container/register-container.component.ts
     * to: @src/app/modules/register/components/register-container/register-container.component
     */
    const classPathToImport = isLibComponent ?
        classPath.split('/src/lib')[1].replace(/\.ts$/, '').replace('/', './') :
        classPath.replace('/apps/next-web', '@').replace(/\.ts$/, '');
    const isNsComponent = options.platformType === global_model_1.PlatformType.Ns || options.platformType === global_model_1.PlatformType.Shared;
    const isWebComponent = options.platformType === global_model_1.PlatformType.Web || options.platformType === global_model_1.PlatformType.Shared;
    const isOnlyNsComponent = options.platformType === global_model_1.PlatformType.Ns;
    const isOnlyWebComponent = options.platformType === global_model_1.PlatformType.Web;
    const isSharedComponent = options.platformType === global_model_1.PlatformType.Shared;
    const importsStringForAngular = `import { Component, ChangeDetectionStrategy } from '@angular/core';\n`;
    const importsStringForWebUtils = isNsComponent ? `import { NsComponent, NsComponentHostLayout } from '@fourpanels/utils';\n` : '';
    return {
        isLibComponent,
        moduleName,
        name,
        dirPath,
        webSelector,
        nsSelector,
        className,
        classPath,
        classPathToImport,
        isNsComponent,
        isWebComponent,
        isOnlyNsComponent,
        isOnlyWebComponent,
        isSharedComponent,
        importsStringForWebUtils,
        importsStringForAngular,
        platformType: options.platformType
    };
};
// ----- Utils -----
function getNameFromOption({ module, name }) {
    return module_util_1.isLibraryModule(module) ? `lib-${strings_1.dasherize(name)}` : strings_1.dasherize(name);
}
//# sourceMappingURL=pars-component-info.js.map