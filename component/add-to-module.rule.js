"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.addToModule = void 0;
const global_model_1 = require("../global-model");
const add_to_array_1 = require("../utility/add-to-array");
const insert_import_util_1 = require("../utility/insert-import.util");
const module_util_1 = require("../utility/module.util");
function addToModule(parsedComponentInfo) {
    return (tree, context) => {
        const { modulePath, moduleDeclarationsArrayName } = module_util_1.validateModuleTypeAndGetModuleInfo(tree, parsedComponentInfo.dirPath, parsedComponentInfo.moduleName, parsedComponentInfo.platformType, global_model_1.GenerateNewFileType.Component);
        add_to_array_1.addToArray(tree, modulePath, moduleDeclarationsArrayName, parsedComponentInfo.className);
        insert_import_util_1.insertImport(tree, modulePath, parsedComponentInfo.className, parsedComponentInfo.classPathToImport);
        return tree;
    };
}
exports.addToModule = addToModule;
;
//# sourceMappingURL=add-to-module.rule.js.map