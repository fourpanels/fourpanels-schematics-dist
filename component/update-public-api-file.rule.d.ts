import { Rule } from "@angular-devkit/schematics";
import { ParsedComponentInfo } from "./component.model";
export declare function updateLibraryPublicApiFile(paredComponentInfo: ParsedComponentInfo): Rule;
