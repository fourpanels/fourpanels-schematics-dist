import { Rule } from '@angular-devkit/schematics';
import { ParsedComponentInfo } from './component.model';
export declare function applyWebTemplate(parsedComponentInfo: ParsedComponentInfo): Rule;
export declare function applyNsTemplate(parsedComponentInfo: ParsedComponentInfo): Rule;
/**
 *  Conditions to run this method:
 *  1- Component has to be jut for nativescript
 *  2- Module is a shared library
 */
export declare function applyNsInSharedLibraryTemplate(parsedComponentInfo: ParsedComponentInfo): Rule;
