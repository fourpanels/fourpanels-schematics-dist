import { Rule } from '@angular-devkit/schematics';
import { ComponentSchema } from './component.model';
export default function (options: ComponentSchema): Rule;
