import { PlatformType } from "../global-model";
export interface ParsedComponentInfo {
    isLibComponent: boolean;
    moduleName: string;
    name: string;
    dirPath: string;
    webSelector: string;
    nsSelector: string;
    className: string;
    classPath: string;
    classPathToImport: string;
    isNsComponent: boolean;
    isWebComponent: boolean;
    isOnlyNsComponent: boolean;
    isOnlyWebComponent: boolean;
    isSharedComponent: boolean;
    importsStringForWebUtils: string;
    importsStringForAngular: string;
    platformType: PlatformType;
}
export interface ComponentSchema {
    /**
     * The declaring NgModule.
     */
    module: string;
    /**
     * The name of the component.
     */
    name: string;
    /**
     * Component will be used in Web, Ns or both platform.
     */
    platformType: PlatformType;
}
