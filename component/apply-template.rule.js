"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.applyNsInSharedLibraryTemplate = exports.applyNsTemplate = exports.applyWebTemplate = void 0;
const schematics_1 = require("@angular-devkit/schematics");
const global_model_1 = require("../global-model");
const file_util_1 = require("../utility/file.util");
const module_util_1 = require("../utility/module.util");
function applyWebTemplate(parsedComponentInfo) {
    return (tree, context) => {
        const templateSource = schematics_1.apply(schematics_1.url('./web-files'), [
            schematics_1.applyTemplates(parsedComponentInfo),
            schematics_1.move(parsedComponentInfo.dirPath)
        ]);
        return schematics_1.mergeWith(templateSource)(tree, context);
    };
}
exports.applyWebTemplate = applyWebTemplate;
;
function applyNsTemplate(parsedComponentInfo) {
    // Component is Jut for web
    if (!parsedComponentInfo.isNsComponent) {
        return (tree, context) => tree;
    }
    return (tree, context) => {
        const templateSource = schematics_1.apply(schematics_1.url('./ns-files'), [
            schematics_1.applyTemplates(parsedComponentInfo),
            schematics_1.move(parsedComponentInfo.dirPath)
        ]);
        return schematics_1.mergeWith(templateSource)(tree, context);
    };
}
exports.applyNsTemplate = applyNsTemplate;
;
/**
 *  Conditions to run this method:
 *  1- Component has to be jut for nativescript
 *  2- Module is a shared library
 */
function applyNsInSharedLibraryTemplate(parsedComponentInfo) {
    // Check condition 1: Component has to be jut for nativescript
    if (!parsedComponentInfo.isOnlyNsComponent) {
        return (tree, context) => tree;
    }
    return (tree, context) => {
        const moduleType = module_util_1.getModulePlatformTypeFromPath(tree, parsedComponentInfo.dirPath, parsedComponentInfo.moduleName);
        // Check condition 2: Module is a shared library
        if (moduleType !== global_model_1.PlatformType.Shared) {
            return tree;
        }
        file_util_1.removeFile(tree, parsedComponentInfo.classPath);
        const templateSource = schematics_1.apply(schematics_1.url('./ns-in-shared-lib-files'), [
            schematics_1.applyTemplates(parsedComponentInfo),
            schematics_1.move(parsedComponentInfo.dirPath)
        ]);
        return schematics_1.mergeWith(templateSource)(tree, context);
    };
}
exports.applyNsInSharedLibraryTemplate = applyNsInSharedLibraryTemplate;
//# sourceMappingURL=apply-template.rule.js.map