"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const schematics_1 = require("@angular-devkit/schematics");
const pars_component_info_1 = require("./pars-component-info");
const add_to_module_rule_1 = require("./add-to-module.rule");
const apply_template_rule_1 = require("./apply-template.rule");
const update_public_api_file_rule_1 = require("./update-public-api-file.rule");
function default_1(options) {
    const paredComponentInfo = pars_component_info_1.parseComponentInfo(options);
    return (tree, context) => {
        return schematics_1.chain([
            apply_template_rule_1.applyWebTemplate(paredComponentInfo),
            apply_template_rule_1.applyNsTemplate(paredComponentInfo),
            apply_template_rule_1.applyNsInSharedLibraryTemplate(paredComponentInfo),
            add_to_module_rule_1.addToModule(paredComponentInfo),
            update_public_api_file_rule_1.updateLibraryPublicApiFile(paredComponentInfo),
        ]);
    };
}
exports.default = default_1;
//# sourceMappingURL=index.js.map