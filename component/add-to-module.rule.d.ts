import { Rule } from "@angular-devkit/schematics";
import { ParsedComponentInfo } from "./component.model";
export declare function addToModule(parsedComponentInfo: ParsedComponentInfo): Rule;
