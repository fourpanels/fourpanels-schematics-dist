"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.applyTemplate = void 0;
const schematics_1 = require("@angular-devkit/schematics");
function applyTemplate(paredComponentInfo) {
    return (tree, context) => {
        const templateSource = schematics_1.apply(schematics_1.url('./files'), [
            schematics_1.applyTemplates(paredComponentInfo),
            schematics_1.move(paredComponentInfo.dirPath)
        ]);
        return schematics_1.mergeWith(templateSource)(tree, context);
    };
}
exports.applyTemplate = applyTemplate;
;
//# sourceMappingURL=apply-template.rule.js.map