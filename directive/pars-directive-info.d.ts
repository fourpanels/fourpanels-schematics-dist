import { DirectiveSchema, ParsedDirectiveInfo } from './directive.model';
export declare const parseDirectiveInfo: (options: DirectiveSchema) => ParsedDirectiveInfo;
