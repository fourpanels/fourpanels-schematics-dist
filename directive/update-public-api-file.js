"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.updateLibraryPublicApiFile = void 0;
const library_public_api_file_util_1 = require("../utility/library-public-api-file.util");
function updateLibraryPublicApiFile(paredDirectiveInfo) {
    // Directive is not library
    if (!paredDirectiveInfo.isLibDirective) {
        return (tree) => tree;
    }
    return (tree) => {
        const pathToExport = `./lib/directives/${paredDirectiveInfo.name}.directive`;
        library_public_api_file_util_1.addExportModuleToLibraryPublicApi(tree, paredDirectiveInfo.moduleName, pathToExport);
        return tree;
    };
}
exports.updateLibraryPublicApiFile = updateLibraryPublicApiFile;
;
//# sourceMappingURL=update-public-api-file.js.map