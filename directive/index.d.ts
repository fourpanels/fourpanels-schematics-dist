import { Rule } from '@angular-devkit/schematics';
import { DirectiveSchema } from './directive.model';
export default function (options: DirectiveSchema): Rule;
