import { PlatformType } from "../global-model";
export interface ParsedDirectiveInfo {
    isLibDirective: boolean;
    moduleName: string;
    name: string;
    dirPath: string;
    selector: string;
    className: string;
    classPath: string;
    classPathToImport: string;
    isNsDirective: boolean;
    isWebDirective: boolean;
    isOnlyNsDirective: boolean;
    isOnlyWebDirective: boolean;
    isSharedDirective: boolean;
    importsStringForAngular: string;
    platformType: PlatformType;
}
export interface DirectiveSchema {
    /**
     * The declaring NgModule.
     */
    module: string;
    /**
     * The name of the component.
     */
    name: string;
    /**
     * Component will be used in Web, Ns or both platform.
     */
    platformType: PlatformType;
}
