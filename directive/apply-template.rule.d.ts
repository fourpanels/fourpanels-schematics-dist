import { Rule } from "@angular-devkit/schematics";
import { ParsedDirectiveInfo } from "./directive.model";
export declare function applyTemplate(paredComponentInfo: ParsedDirectiveInfo): Rule;
