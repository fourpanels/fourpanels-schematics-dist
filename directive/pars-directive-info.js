"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.parseDirectiveInfo = void 0;
const strings_1 = require("@angular-devkit/core/src/utils/strings");
const global_model_1 = require("../global-model");
const module_util_1 = require("../utility/module.util");
exports.parseDirectiveInfo = (options) => {
    const isLibDirective = module_util_1.isLibraryModule(options.module);
    const moduleName = module_util_1.getModuleNameFromOption(options);
    const name = getNameFromOption(options);
    const dirPath = isLibDirective ?
        `/libs/${moduleName}/src/lib/directives/` :
        `/apps/next-web/app/modules/${moduleName}/directives/`;
    const selector = isLibDirective ? `lib${strings_1.classify(options.name)}` : `app${strings_1.classify(options.name)}`;
    const className = `${strings_1.classify(name)}Directive`;
    const classPath = `${dirPath}/${name}.directive.ts`;
    /**
     * Library
     * from: /libs/lib-button/src/lib/directives/lib-primary-button/lib-primary-button.directive.ts
     * to: ./directives/lib-primary-button/lib-primary-button.directive
     *
     * Module
     * from: /apps/next-web/app/modules/register/directives/register-container.directive.ts
     * to: @src/app/modules/register/directives/register-container.directive
     */
    const classPathToImport = isLibDirective ?
        classPath.split('/src/lib')[1].replace(/\.ts$/, '').replace('/', './') :
        classPath.replace('/apps/next-web', '@').replace(/\.ts$/, '');
    const isNsDirective = options.platformType === global_model_1.PlatformType.Ns || options.platformType === global_model_1.PlatformType.Shared;
    const isWebDirective = options.platformType === global_model_1.PlatformType.Web || options.platformType === global_model_1.PlatformType.Shared;
    const isOnlyNsDirective = options.platformType === global_model_1.PlatformType.Ns;
    const isOnlyWebDirective = options.platformType === global_model_1.PlatformType.Web;
    const isSharedDirective = options.platformType === global_model_1.PlatformType.Shared;
    const importsStringForAngular = `import { Directive } from '@angular/core';\n`;
    return {
        isLibDirective,
        moduleName,
        name,
        dirPath: dirPath,
        selector,
        className,
        classPath,
        classPathToImport,
        isNsDirective,
        isWebDirective,
        isOnlyNsDirective,
        isOnlyWebDirective,
        isSharedDirective,
        importsStringForAngular,
        platformType: options.platformType
    };
};
// ----- Utils -----
function getNameFromOption({ module, name }) {
    return module_util_1.isLibraryModule(module) ? `lib-${strings_1.dasherize(name)}` : strings_1.dasherize(name);
}
//# sourceMappingURL=pars-directive-info.js.map